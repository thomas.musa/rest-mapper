<?php

namespace Drupal\rest_mapper\Event;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Use this event object to share the entity to append to response cache.
 *
 * @package Drupal\rest_mapper\Event
 */
class AppendCacheMetadataToResponseEvent extends Event {

  const EVENT_NAME = 'rest_mapper.add_cache_dependency';

  /**
   * Request object used in notify controller.
   *
   * @var \Drupal\Core\Cache\CacheableDependencyInterface
   */
  private $dependency;

  /**
   * Array of cache tags.
   *
   * @var string[]
   */
  private $tags;

  /**
   * AppendCacheMetadataToResponseEvent constructor.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $dependency
   *    Entity to append to cache.
   * @param string[] $tags
   *    Array of cache tags.
   */
  public function __construct(CacheableDependencyInterface $dependency = NULL, array $tags = []) {
    $this->dependency = $dependency;
    $this->tags = $tags;
  }

  /**
   * Returns the dependency to cache.
   *
   * @return \Drupal\Core\Cache\CacheableDependencyInterface
   *    The dependency to cache.
   */
  public function getCacheableDependency() {
    return $this->dependency;
  }

  /**
   * Returns the dependency to cache.
   *
   * @return string[]
   *    Array of cache tags.
   */
  public function getCacheTags() {
    return $this->tags;
  }

}
