<?php

namespace Drupal\rest_mapper\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Tells the controller the response may contain proposals.
 *
 * @package Drupal\rest_mapper\Event
 */
class ResponseContainsProposalEvent extends Event {

  const EVENT_NAME = 'rest_mapper.response_contains_proposal';

  /**
   * ResponseContainsProposalEvent constructor.
   */
  public function __construct() {}

}
