<?php

namespace Drupal\rest_mapper\EventSubscriber;

use Drupal\rest_mapper\Event\AppendCacheMetadataToResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ControllerDataEventSubscriber.
 */
class ControllerDataEventSubscriber implements EventSubscriberInterface {

  /**
   * Array of entities to append as cache dependencies.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $additionalCacheDependencies = [];

  /**
   * Array of entities to append as cache dependencies.
   *
   * @var string[]
   */
  protected $additionalCacheTags = [];


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AppendCacheMetadataToResponseEvent::EVENT_NAME] = ['appendCacheMetadataToResponse'];

    return $events;
  }

  /**
   * Event subscriber callback.
   *
   * @param \Drupal\rest_mapper\Event\AppendCacheMetadataToResponseEvent $event
   *    The event to use for this callback.
   */
  public function appendCacheMetadataToResponse(AppendCacheMetadataToResponseEvent $event) {
    if (!empty($event->getCacheableDependency())) {
      $this->additionalCacheDependencies[] = $event->getCacheableDependency();
    }

    if (!empty($event->getCacheTags())) {
      // Deduplication will occur when getting the tags; no need to do it here.
      $this->additionalCacheTags = array_merge($this->additionalCacheTags, $event->getCacheTags());
    }
  }

  /**
   * Gets entities ready for cache dependencies for a response object.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *    The array of entities.
   */
  public function getAdditionalCacheDependencies() {
    return $this->additionalCacheDependencies;
  }

  /**
   * Gets tags of dependencies of the current response.
   *
   * Removes duplicates.
   *
   * @return array
   *    Array of cache tags.
   */
  public function getAdditionalCacheDependenciesTags() {
    $tags = [];

    foreach ($this->additionalCacheDependencies as $additionalCacheDependency) {
      $tags = array_merge($tags, $additionalCacheDependency->getCacheTags());
    }

    // Remove duplicate values.
    $tags = array_values(array_unique($tags));

    return $tags;
  }

  /**
   * Gets additional tags for a response object.
   *
   * @return array
   *    Array of cache tags.
   */
  public function getAdditionalCacheTags() {
    return $this->additionalCacheTags;
  }
}
