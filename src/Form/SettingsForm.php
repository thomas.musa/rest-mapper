<?php

namespace Drupal\rest_mapper\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  protected $fileSystem;

  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $file_system, MessengerInterface $messenger) {


    parent::__construct($config_factory);
    $this->fileSystem = $file_system;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'rest_mapper.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('redirect_2_front.settings');
    $form = parent::buildForm($form, $form_state);
    $default_value = !empty($config->get('path_mapping')) ? $config->get('path_mapping') : '../rest';
    $form['path_mapping'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mapping path'),
      '#default_value' => $default_value,
      '#required' => TRUE,
      '#description' => $this->t('Set the path where you store your mapping rest.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $path = $form_state->getValue('path_mapping');
    $directory = $path . '/Mapping';
    // Check if directory exists.
    if (!is_dir($directory)) {
      if ($this->fileSystem->mkdir($directory, NULL, TRUE) == FALSE) {
        $form_state->setErrorByName('path_mapping', t("The directory doesn't exist and can't be created"));
      }
    }
    $files = file_scan_directory($path . '/Mapping', '/.*\.field_mapping\.yml$/', ['recurse' => 1]);
    if (empty($files)) {
      $this->messenger->addWarning(t('The directory exist but it is empty'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('rest_mapper.settings')
      ->set('path_mapping', $form_state->getValue('path_mapping'))
      ->save();
  }

}
