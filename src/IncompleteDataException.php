<?php

namespace Drupal\rest_mapper\Exception;

/**
 * Triggered when a content has not all required content set by the user.
 *
 * @package Drupal\rest_mapper\Exception
 */
class IncompleteDataException extends \Exception {
}
