<?php

namespace Drupal\rest_mapper\Normalizer\Entity;

use Drupal\rest_mapper\Normalizer\RestMapperNormalizer;


/**
 * Class VocabularyEntityNormalizer.
 *
 * @package Drupal\rest_mapper\Normalizer\Entity
 */
class EntityNormalizer extends RestMapperNormalizer {

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var array
   */
  protected $supportedInterfaceOrClass = 'Drupal\Core\Entity\EntityInterface';

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    return parent::normalize($object, $format, $context);
  }

}
