<?php

namespace Drupal\rest_mapper\Normalizer\Field;

use Drupal\rest_mapper\Normalizer\RestMapperNormalizer;

/**
 * Class FieldEntityReferenceNormalizer.
 *
 * @package Drupal\rest_mapper\Normalizer\Field
 */
class FieldEntityReferenceNormalizer extends RestMapperNormalizer {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem';

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $object */
    /** @var \Drupal\Core\Entity\Plugin\DataType\EntityReference $entity_reference */
    $entity_reference = $object->get('entity');
    /** @var \Drupal\Core\TypedData\TypedDataInterface $target */
    $target = $entity_reference->getTarget();
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = isset($target) ? $target->getValue() : NULL;
    return parent::normalize($entity, $format, $context);
  }

}
