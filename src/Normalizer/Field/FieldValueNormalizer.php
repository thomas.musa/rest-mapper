<?php

namespace Drupal\rest_mapper\Normalizer\Field;

use Drupal\Core\Field\Plugin\Field\FieldType\MapItem;
use Drupal\Core\Field\Plugin\Field\FieldType\StringLongItem;
use Drupal\rest_mapper\Normalizer\RestMapperNormalizer;

/**
 * Class FieldValueNormalizer.
 *
 * @package Drupal\rest_mapper\Normalizer\Field
 */
class FieldValueNormalizer extends RestMapperNormalizer {

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var array
   */
  protected $supportedInterfaceOrClass = ['Drupal\Core\Field\FieldItemInterface'];

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    if (get_class($object) == StringLongItem::class) {
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\StringLongItem $object */
      if ($object->getFieldDefinition()->getName() == 'field_text') {
        return json_decode($object->getValue()['value']);
      }
    }
    /** @var \Drupal\Core\Field\FieldItemInterface $object */
    elseif (get_class($object) == MapItem::class) {
      return $object->getValue();
    }
    return $object->getValue()['value'];
  }

}
