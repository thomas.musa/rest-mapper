<?php

namespace Drupal\rest_mapper\Normalizer;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest_mapper\Event\AppendCacheMetadataToResponseEvent;
use Drupal\rest_mapper\Service\NormalizerTools;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Class RestMapperNormalizer.
 *
 * @package Drupal\rest_mapper\Normalizer
 */
abstract class RestMapperNormalizer extends NormalizerBase {

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string|array
   */
  protected $supportedInterfaceOrClass = 'Drupal\Core\Entity\EntityInterface';

  /**
   * The entity type manage instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Core language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Normalizer tools service.
   *
   * @var \Drupal\rest_mapper\Service\NormalizerTools
   */
  protected $normalizerTools;



  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Current user service. (AccountProxyInterface object)
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  protected $langcode;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, NormalizerTools $normalizer_tools, EventDispatcherInterface $event_dispatcher, AccountProxyInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->normalizerTools = $normalizer_tools;
    $this->eventDispatcher = $event_dispatcher;
    $this->currentUser = $current_user;
  }

  /**
   * Normalizes an object into a set of arrays/scalars.
   *
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    if (empty($object)) {
      return [];

    }
    // Make this entity a dependency of the current response.
    if ($object instanceof CacheableDependencyInterface) {
      $this->appendCacheDependencyToResponse($object);
    }

    $lang_code = $this->languageManager->getDefaultLanguage()->getId();
    if (!empty($context['lang'])) {
      $lang_code = $context['lang'];
    }
    if (!empty($context['langcode'])) {
      $lang_code = $context['langcode'];
    }
    $this->langcode = $lang_code;
    // Get entity translate.
    if (!empty($lang_code) && $object instanceof ContentEntityInterface && $object->isTranslatable() && $object->hasTranslation($lang_code)) {
      $object = $object->getTranslation($lang_code);
    }
    $field_mapping = $this->normalizerTools->getFieldMapping($object->getEntityTypeId(), $object->bundle(), $context);


    if (empty($field_mapping)) {
      $this->buildAutoMapping($object);
    }
    if (empty($field_mapping)) {
      return [];
    }

    $data = $this->normalizeWithFieldMapping($field_mapping, $object, $format, $context);

    return $data;
  }

  private function buildAutoMapping($object) {
    $field_mapping = [];
    // Default field mappings by field definitions.
    if ($object instanceof ContentEntityInterface) {
      $mapping = [];
      $camel_case_normalizer = new CamelCaseToSnakeCaseNameConverter();
      /* @var  $fields_definitions \Drupal\Core\Field\BaseFieldDefinition[] */
      $fields_definitions = $object->getFieldDefinitions();
      foreach ($fields_definitions as $field_name => $field_definition) {
        $mapping[$camel_case_normalizer->denormalize($field_name)] = $field_name;
      }
      $field_mapping['mapping'] = $mapping;
    }
    else {
      // Build default field mapping for config entities.
      $entity_type_definition = $this->entityTypeManager->getDefinition($object->getEntityTypeId());
      foreach ($entity_type_definition->get('config_export') as $config_export) {
        if (!isset($fields_options[$config_export])) {
          $field_mapping[$config_export] = $config_export;
        }
      }
      foreach ($entity_type_definition->get('entity_keys') as $key_options => $options) {
        if (!empty($options)) {
          $field_mapping[$options] = $key_options;
        }
        else {
          $field_mapping[$key_options] = $key_options;
        }
      }
    }
    return $field_mapping;
  }

  /**
   * Normalize entity with field mapping.
   *
   * @param array $field_mapping
   *   The field mapping of entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The content entity to normalize.
   * @param string|null $format
   *   Return format, e.g.: 'json'.
   * @param array $context
   *   Contains context values. Useful for preventing infinite loops by
   *   controlling already loaded parents.
   *
   * @return array|bool|float|int|mixed|string
   *   Normalized data as array.
   */
  private function normalizeWithFieldMapping(array $field_mapping, EntityInterface $entity, $format = NULL, array $context = []) {
    $data = [];
    // Mapping is empty.
    if (empty($field_mapping['mapping'])) {
      return $data;
    }
    // Drupal\taxonomy\Entity\Vocabulary
    // Loop on mapping.
    foreach ($field_mapping['mapping'] as $field_alias => $field_name) {
      // Extract the view mode from the field name, if any.
      $view_mode = $this->extractViewModeFromFieldName($field_name, $extracted_field_name);

      if (empty($context['view_mode'])) {
        // Overwrite the view mode in the context.
        $context['view_mode'] = $view_mode;
      }
      else {

        $view_mode = $context['view_mode'];
      }
      if ($entity instanceof ContentEntityInterface && $entity->hasField($extracted_field_name) == FALSE) {
        continue;
      }
      /* @var \Drupal\Core\Field\FieldItemListInterface $field */
      $field = $entity->get($extracted_field_name);

      if (empty($field) || ($field instanceof FieldItemListInterface && $field->isEmpty())) {
        continue;
      }

      if (!empty($context['parent'][$extracted_field_name][$entity->getEntityTypeId()][$view_mode]) && in_array($entity->id(), $context['parent'][$extracted_field_name][$entity->getEntityTypeId()][$view_mode], TRUE)) {
        // The entity has already been loaded in a parent. Do not load it to
        // prevent infinite inclusions.
        if ($field_alias == 'none') {
          $data = $field->getString();
        }
        else {
          $data[$field_alias] = $field->getString();
        }
        continue;
      }

      // Set the new context for this field.
      $new_context = $context;
      $new_context['parent'][$extracted_field_name][$entity->getEntityTypeId()][$view_mode][] = $entity->id();
      if (is_string($field)) {
        $normalized_object = $field;
      }
      else {
        // Get cardinality of the field.
        $cardinality = $field->getFieldDefinition()
          ->getFieldStorageDefinition()
          ->getCardinality();

        // Normalize depending cardinality of field.
        if ($cardinality > 1 || $cardinality == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
          $normalized_object = [];
          foreach ($field as $field_value) {
            $normalized_object[] = $this->serializer->normalize($field_value, $format, $new_context);
          }
        }
        else {
          $normalized_object = $this->serializer->normalize($field, $format, $new_context);
        }
      }

      // Set data with field alias.
      if ($field_alias == 'none') {
        $data = $normalized_object;
      }
      else {
        $data[$field_alias] = $normalized_object;
      }
    }

    return $data;
  }

  /**
   * Extracts view mode from real field name.
   *
   * For example, a field name in the mapping file can be named:
   * "field_discover_other_pratrices:proposal"; the part before the colon is
   * the real field name, the second part is the view mode.
   *
   * @param string $field_name
   *    The entire field name from mapping.
   * @param string $extracted_field_name
   *    Passed by reference variable which will contain the field name without
   *    the view mode if any.
   *
   * @return string
   *    The view mode if any, returns 'default' if none.
   */
  protected function extractViewModeFromFieldName($field_name, &$extracted_field_name) {
    $parts = explode(':', $field_name);

    if (count($parts) !== 2) {
      // Return the field name as is.
      $extracted_field_name = $field_name;
      return 'default';
    }

    $extracted_field_name = $parts[0];
    return $parts[1];
  }

  /**
   * Appends an entity as cache dependency on the response object.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $dependency
   *    The entity to append.
   */
  protected function appendCacheDependencyToResponse(CacheableDependencyInterface $dependency) {
    $this->eventDispatcher->dispatch(
      AppendCacheMetadataToResponseEvent::EVENT_NAME,
      new AppendCacheMetadataToResponseEvent($dependency));
  }



}
