<?php

namespace Drupal\rest_mapper\Service;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class NormalizerTools.
 */
class NormalizerTools {

  /**
   * Field mapping array.
   *
   * @var array
   */
  protected $fieldMapping = [];

  /**
   * Cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  protected $configFactory;

  /**
   * NormalizerTools constructor.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $cache_tags_invalidator
   *    Cache tags invalidator service.
   */
  public function __construct(CacheTagsInvalidator $cache_tags_invalidator, ConfigFactoryInterface $config_factory) {
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->configFactory = $config_factory;
  }

  /**
   * Helper to retrieve field mapping settings.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The entity bundle.
   * @param array $context
   *   The context of the normalizing.
   *
   * @return array
   *   The entity mapping array.
   */
  public function getFieldMapping($entity_type_id, $bundle, array $context) {
    $view_mode = $context['view_mode'] ?? 'default';

    $conf = $this->configFactory->get('rest_mapper.settings');
    // @todo caching mapping!
    if (!empty($this->fieldMapping[$entity_type_id][$bundle][$view_mode])) {
      return $this->fieldMapping[$entity_type_id][$bundle][$view_mode];
    }
    $config_name = $entity_type_id . '.' . $bundle . '.field_mapping.yml';
    $config_name_view_mode = $entity_type_id . '.' . $bundle . '.' . $view_mode . '.field_mapping.yml';
    $path = $conf->get('path_mapping');
    $files = file_scan_directory($path . '/Mapping', '/.*\.field_mapping\.yml$/', ['recurse' => 1]);

    // Search file with view mode.
    foreach ($files as $file) {
      if ($file->filename == $config_name_view_mode) {
        $mapping = Yaml::parse(file_get_contents($file->uri));
        $this->fieldMapping[$entity_type_id][$bundle][$view_mode] = $mapping;
        return $mapping;
      }
    }
    // Search default mapping.
    foreach ($files as $file) {
      if ($file->filename == $config_name) {

        $mapping = Yaml::parse(file_get_contents($file->uri));
        $this->fieldMapping[$entity_type_id][$bundle][$view_mode] = $mapping;
        return $mapping;
      }
    }
    return NULL;
  }

  /**
   * Tells if a field mapping has a key set in it.
   *
   * @param string $key_mapping
   *    The key to look for.
   * @param array $field_mapping
   *    Field mapping to look into. You can get a field mapping with
   *    getFieldMapping().
   *
   * @return bool
   *    TRUE if key is in the field mapping, FALSE otherwise.
   */
  public function containsFieldNameInFieldMapping($key_mapping, array $field_mapping) {
    if (empty($field_mapping['mapping'])) {
      return FALSE;
    }

    return in_array($key_mapping, $field_mapping['mapping'], TRUE);
  }

}
